use rand::Rng;
use std::io::{self, stdout, Read, Write};
use termion::{
    self,
    raw::{IntoRawMode, RawTerminal},
};

fn main() {
    let stdout = stdout().into_raw_mode().unwrap();
    let stdin = termion::async_stdin().bytes();
    let mut grid = Grid::new(15, 45);
    grid._randomize();
    // Planeur:
    //grid.cells[0][1] = CellState::Alive;
    //grid.cells[1][2] = CellState::Alive;
    //grid.cells[2][0] = CellState::Alive;
    //grid.cells[2][1] = CellState::Alive;
    //grid.cells[2][2] = CellState::Alive;
    // HWSS:
    //grid.cells[0][0] = CellState::Alive;
    //grid.cells[0][3] = CellState::Alive;
    //grid.cells[1][4] = CellState::Alive;
    //grid.cells[2][0] = CellState::Alive;
    //grid.cells[2][4] = CellState::Alive;
    //grid.cells[3][1] = CellState::Alive;
    //grid.cells[3][2] = CellState::Alive;
    //grid.cells[3][3] = CellState::Alive;
    //grid.cells[3][4] = CellState::Alive;

    match run_app(grid, stdout, stdin) {
        Err(e) => println!("An error as occured: {}", e),
        Ok(_) => {}
    }
}

fn run_app(
    mut grid: Grid,
    mut stdout: RawTerminal<io::Stdout>,
    mut stdin: io::Bytes<termion::AsyncReader>,
) -> io::Result<()> {
    stdout.flush()?;
    write!(stdout, "{}{}", termion::clear::All, termion::cursor::Hide)?;

    loop {
        // End the program if the user press 'q'
        let b = stdin.next();
        if let Some(Ok(b'q')) = b {
            write!(stdout, "{}", termion::cursor::Restore)?;
            break;
        }

        // End the program if all the cells in the grid are dead
        if grid.all_dead() {
            break;
        }

        grid.draw();
        grid.next_generation();
        std::thread::sleep(std::time::Duration::from_millis(300));
    }

    Ok(())
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum CellState {
    Dead,
    Alive,
}

struct Pos {
    row: usize,
    col: usize,
}

#[derive(Debug)]
struct Grid {
    cols: usize,
    _rows: usize,
    cells: Vec<Vec<CellState>>,
}

impl Grid {
    fn new(rows: usize, cols: usize) -> Self {
        Grid {
            cols,
            _rows: rows,
            cells: vec![vec![CellState::Dead; cols as usize]; rows as usize],
        }
    }

    fn _fill(&mut self, state: CellState) {
        for row in &mut self.cells {
            *row = vec![state; self.cols as usize];
        }
    }

    fn draw(&self) {
        let offset_rows = 2;
        let offset_cols = 2;

        for (i, row) in self.cells.iter().enumerate() {
            print!(
                "{}",
                termion::cursor::Goto(offset_rows, (i + offset_cols) as u16),
            );

            row.iter().for_each(|cell| {
                print!(
                    "{}",
                    match cell {//░▒
                        CellState::Dead => '▒',
                        CellState::Alive => '█',
                    }
                );
            });

            println!();
        }
    }

    fn _randomize(&mut self) {
        let mut rng = rand::thread_rng();

        for row in &mut self.cells {
            for cell in row {
                *cell = match rng.gen_range(0..2) {
                    1 => CellState::Alive,
                    0 | _ => CellState::Dead,
                };
            }
        }
    }

    fn all_dead(&self) -> bool {
        self.cells
            .iter()
            .all(|row| row.iter().all(|&cell| cell == CellState::Dead))
    }

    fn check_neighbors(&self, pos: Pos) -> usize {
        // flatten -> 2d
        // get the row:
        // floor(index / (rows - 1))
        // get the col:
        // index - row * cols
        // 2d -> flatten:
        // row * cols + col
        let grid: Vec<CellState> = self.cells.iter()
            .flatten()
            .cloned()
            .collect();
        let mut alive_neighbors = 0;
        let mut inc_if_alive = |x| {
            if x == CellState::Alive {
                alive_neighbors += 1;
            }
        };

        let index = (pos.row * self.cols + pos.col) as usize;
        let mut neighbors = vec![
            -((self.cols + 1) as i32),
            -(self.cols as i32),
            -1, 1,
            self.cols as i32,
            (self.cols + 1) as i32,
        ];
        if index != 0 && index % self.cols != 0 {
            neighbors.push((self.cols - 1) as i32);
        }
        if index != 0 && (index + 1) % self.cols != 0 {
            neighbors.push(-((self.cols - 1) as i32));
        }

        neighbors.into_iter().for_each(|neighbor| {
            if let Some(state) = try_access_index(&grid, index as i32 + neighbor) {
                inc_if_alive(*state);
            }
        });

        alive_neighbors
    }

    fn next_generation(&mut self) {
        // flatten -> 2d
        // get the row:
        // floor(index / (rows - 1))
        // get the col:
        // index - row * cols
        // 2d -> flatten:
        // row * cols + col
        let grid: Vec<CellState> = self.cells.iter()
            .flatten()
            .cloned()
            .collect();
        let mut new_grid = self.cells.clone();

        for (i, cell) in grid.iter().enumerate() {
            let row = i / self.cols;
            let col = i - row * self.cols;
            let pos = Pos { row, col };

            match cell {
                CellState::Alive => {
                    let alive_neighbors = self.check_neighbors(pos);
                    if alive_neighbors < 2 || alive_neighbors > 3 {
                        new_grid[row][col] = CellState::Dead;
                    }
                },
                CellState::Dead => {
                    if self.check_neighbors(pos) == 3 {
                        new_grid[row][col] = CellState::Alive;
                    }
                },
            }
        }
        self.cells = new_grid;
    }
}

fn try_access_index<'a, T>(arr: &'a Vec<T>, i: i32) -> Option<&'a T> {
    if i >= arr.len() as i32 || i < 0 {
        return None;
    }

    Some(&arr[i as usize])
}
